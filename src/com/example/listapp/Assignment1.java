package com.example.listapp;


import android.app.*;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.content.*;

public class Assignment1 extends Activity{
	
	TextView tv;
	Button add;
	Button sub;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.ass1);
		tv = (TextView) findViewById(R.id.textView1);
		add = (Button) findViewById(R.id.add);
		sub = (Button) findViewById(R.id.subtract);
		add.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				//String i = tv.getText().toString();
				int k = Integer.parseInt(tv.getText().toString());
				k += 1;
				String s = Integer.toString(k);
				tv.setText(s);
			}
		});
		sub.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				//String i = tv.getText().toString();
				int k = Integer.parseInt(tv.getText().toString());
				k -= 1;
				String s = Integer.toString(k);
				tv.setText(s);
			}
		});
	}
	public void home(View v){
		Intent i=new Intent(Assignment1.this, listClass.class);
		startActivity(i);
		
	}
}
