package com.example.listapp;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class sendSMS extends Activity {

	EditText t1, t2;
	Button b;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.sms);
		t1 = (EditText) findViewById(R.id.num);
		t2 = (EditText) findViewById(R.id.sub);
		b = (Button) findViewById(R.id.send);
		b.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String num = t1.getText().toString();
				String sub = t2.getText().toString();
				if(num != "" && sub != "")
					sendMsg(num, sub);
				else 
					Toast.makeText(getBaseContext(), "Entery invalid", Toast.LENGTH_SHORT).show();
			}
		});
	}

	public void sendMsg(String num, String sub) {
		// TODO Auto-generated method stub
		String sent = "SMS_SENT";
		String del = "SMS_DEL";
		PendingIntent sentPI = PendingIntent.getActivity(this, 0, new Intent(sent), 0);
		PendingIntent delPI = PendingIntent.getActivity(this, 0, new Intent(del), 0);
		registerReceiver(new BroadcastReceiver() {
			
			@Override
			public void onReceive(Context context, Intent intent) {
				// TODO Auto-generated method stub
				switch(getResultCode()){
				case Activity.RESULT_OK:
					Toast.makeText(getBaseContext(), "SMS SENT", Toast.LENGTH_SHORT).show();
					break;
				case SmsManager.RESULT_ERROR_RADIO_OFF:
					Toast.makeText(getBaseContext(), "RADIO OFF", Toast.LENGTH_SHORT).show();
					break;
				case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
					Toast.makeText(getBaseContext(), "GENERIC FAILURE", Toast.LENGTH_SHORT).show();
					break;
				case SmsManager.RESULT_ERROR_NO_SERVICE:
					Toast.makeText(getBaseContext(), "NO SERVICE", Toast.LENGTH_SHORT).show();
					break;
				}
			}
		}, new IntentFilter(sent));
		
		registerReceiver(new BroadcastReceiver() {
			
			@Override
			public void onReceive(Context context, Intent intent) {
				// TODO Auto-generated method stub
				switch(getResultCode()){
				case Activity.RESULT_OK:
					Toast.makeText(getBaseContext(), "SMS DELIVERED", Toast.LENGTH_SHORT).show();
					break;
				case Activity.RESULT_CANCELED:
					Toast.makeText(getBaseContext(), "SMS NOT DELIVERED", Toast.LENGTH_SHORT).show();
					break;
				}
			}
		}, new IntentFilter(del));
		
		SmsManager sm = SmsManager.getDefault();
		sm.sendTextMessage(num, null, sub, sentPI, delPI);
	}
	
}
