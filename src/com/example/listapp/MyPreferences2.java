package com.example.listapp;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MyPreferences2 extends Activity {
	
	TextView tv;
	Button b;
	LinearLayout ll;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.preflay);
		tv = (TextView) findViewById(R.id.tv1);
		b = (Button) findViewById(R.id.button1);
		ll = (LinearLayout) findViewById(R.id.layout);
		b.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(MyPreferences2.this, MyPreferences.class);
				startActivityForResult(i, 0);
			}
		});
		checkPref();
	}
	
	public void checkPref(){
		SharedPreferences mypref = PreferenceManager.getDefaultSharedPreferences(MyPreferences2.this);
		String red = "" + mypref.getBoolean("red", false);
		String green = "" + mypref.getBoolean("green", false);
		if(red.equals("true")){
			ll.setBackgroundColor(Color.RED);
			tv.setText("RED");
		}
		else if(green.equals("true")){
			ll.setBackgroundColor(Color.GREEN);
			tv.setText("GREEN");
		}
		else{
			ll.setBackgroundColor(Color.BLUE);
			tv.setText("No Preference Set");
		}
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		checkPref();
	}
}
