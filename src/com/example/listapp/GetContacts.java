package com.example.listapp;

import java.util.ArrayList;

import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.ContactsContract.PhoneLookup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;

public class GetContacts extends Activity {

	TextView tv;
	ArrayList<String> ar;
	AutoCompleteTextView actv;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.contact);
		tv = (TextView) findViewById(R.id.textView1);
		tv.setScrollbarFadingEnabled(true);
		actv = (AutoCompleteTextView) findViewById(R.id.autoCompleteTextView1);
		ar = new ArrayList<String>();
		Cursor c = getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
		while(c.moveToNext()){
			int index = c.getColumnIndex(PhoneLookup.DISPLAY_NAME);
			String people = c.getString(index);
			ar.add(people);
			tv.append("" + index + " :" + people + "\n");
		}
		ArrayAdapter<String> aa = new ArrayAdapter<String>(this, android.R.layout.simple_expandable_list_item_1, ar);
		actv.setAdapter(aa);
		actv.setThreshold(0);
	}
}
