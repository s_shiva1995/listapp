package com.example.listapp;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
//import android.os.CountDownTimer;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class listClass extends ListActivity {

	String[] myArray = {"Trial","Assignment1","MyWallpaper","login","RandomBack","MyMusicActivity","Toggle","Radio","Seekbar",
			"Progress","AutoCompleteEx","SpinnerEx","shopping_splash","MyBindActivity","MyBat","MyNotification","InsertData",
			"MyPreferences2","sendSMS","GetContacts","GetAnimationEx","MyViewShow","AccelerometerSensor","LightSensor"
			,"ProximitySensor"};
	private MyGetDatabase2 db = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setListAdapter(new ArrayAdapter<String>(this,android.R.layout.simple_expandable_list_item_1,myArray));
		
		db = new MyGetDatabase2(this, "9000series.sqlite");
		db.createDatabase("/data/data/com.example.listapp/databases/", "9000series.sqlite");
		db.createDatabase("/data/data/com.example.listapp/databases/", "8000series.sqlite");
		db.createDatabase("/data/data/com.example.listapp/databases/", "7000series.sqlite");
		
		//use above

//		MyGetDatabase myDbHelper = new MyGetDatabase(null);
//        myDbHelper = new MyGetDatabase(this);
//        try {
// 
//        	myDbHelper.createDataBase();
// 
// 	} catch (IOException ioe) {
// 
// 		throw new Error("Unable to create database");
// 
// 	}
// 
// 	try {
// 
// 		myDbHelper.openDataBase();
// 
// 	}catch(SQLException sqle){
// 
// 		throw sqle;
// 
// 	}
		
	}
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		String myData = myArray[position];
		try {
			
			Class myClass = Class.forName("com.example.listapp."+myData);
			Intent i = new Intent(listClass.this, myClass);
			startActivity(i);
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
