package com.example.listapp;

import android.app.Activity;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.widget.TextView;

public class LightSensor extends Activity implements SensorEventListener {

	Sensor s;
	SensorManager sm;
	TextView tv;
	private StringBuilder msg = new StringBuilder(2048);
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.sensor);
		tv = (TextView) findViewById(R.id.textView1);
		sm = (SensorManager) getSystemService(SENSOR_SERVICE);
		s = sm.getDefaultSensor(Sensor.TYPE_LIGHT);
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		sm.registerListener(this, s, SensorManager.SENSOR_DELAY_NORMAL);
	}
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		sm.unregisterListener(this);
	}
	
	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// TODO Auto-generated method stub
		msg.insert(0, s.getName() + "Accuracy changed: " + accuracy + (accuracy == 1?"(LOW)":(accuracy == 2?"(MED)":"(HIGH)"))+"\n");
		tv.setText(msg);
		tv.invalidate();
	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		// TODO Auto-generated method stub
		msg.insert(0, "Got a sensor event: " + event.values[0] + "SI lux units\n");
		tv.setText(msg);
		tv.invalidate();
	}

}
