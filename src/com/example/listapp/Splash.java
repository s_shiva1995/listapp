package com.example.listapp;

import android.app.Activity;
import android.os.Bundle;
import android.widget.*;
import android.content.Intent;

public class Splash extends Activity {

	ProgressBar pb;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.splash);
		pb = (ProgressBar) findViewById(R.id.progressBar1);
		pb.postDelayed(new Runnable() {
			
			@Override
			public void run(){
				Intent i = new Intent(Splash.this, listClass.class);
				startActivity(i);
				finish();
			}
		} , 3000);
		
	}
	
}
