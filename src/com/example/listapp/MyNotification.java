package com.example.listapp;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MyNotification extends Activity {

	NotificationManager nm;
	Button b;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.notify);
		nm = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
		b = (Button)findViewById(R.id.button1);
		b.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				Notification n = new Notification(android.R.drawable.stat_notify_chat, "Alert", System.currentTimeMillis());
				Intent i = new Intent(MyNotification.this, MyNotification.class);
				PendingIntent p = PendingIntent.getActivity(MyNotification.this, 0, i, 0);
				n.defaults = Notification.DEFAULT_SOUND;
				n.defaults = Notification.DEFAULT_LIGHTS;
				n.defaults = Notification.DEFAULT_VIBRATE;
				n.setLatestEventInfo(MyNotification.this, "h", "l", p);
				nm.notify(0, n);
			}
		});
		
	}
	
}
