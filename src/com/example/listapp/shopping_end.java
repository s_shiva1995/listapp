package com.example.listapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class shopping_end extends Activity{
	
	TextView tv;
	Button b;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.shopping3);
		tv = (TextView) findViewById(R.id.textView2);
		Intent i = getIntent();
		int bill = i.getIntExtra("bill", 1);
		tv.setText("Your Bill is " + bill);
		b = (Button) findViewById(R.id.home);
		b.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(shopping_end.this, listClass.class);
				startActivity(i);
			}
		});
		
	}

}
