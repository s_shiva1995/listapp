package com.example.listapp;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Locale;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class MyGetDatabase2 extends SQLiteOpenHelper{

//	private static final String DATABASE_PATH = "/data/data/com.example.listapp/databases/";
//	private static final String DATABASE_NAME = "9000series.sqlite";
	
	private static final int SCHEMA_VERSION = 1;
	
	public SQLiteDatabase dbSqlite;
	
	private final Context myContext;
	
	public MyGetDatabase2(Context context, String name) {
		super(context, name, null, SCHEMA_VERSION);
		// TODO Auto-generated constructor stub
		this.myContext = context;
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		
	}
	
	public void createDatabase(String DatabasePath, String DatabaseName) {
		createDB(DatabasePath, DatabaseName);
	}	

	private void createDB(String DatabasePath, String DatabaseName){
		
		boolean dbExist = DBExist(DatabasePath, DatabaseName);
		
		if(!dbExist){
			this.getReadableDatabase();
			copyDBFromResources(DatabasePath, DatabaseName);
		}
	}
	
	private boolean DBExist(String DatabasePath, String DatabaseName){
		
		SQLiteDatabase db = null;
		String DATABASE_PATH = DatabasePath;
		String DATABASE_NAME = DatabaseName;
		
		try{
			String databasePath = DATABASE_PATH + DATABASE_NAME;
			db = SQLiteDatabase.openDatabase(databasePath, null, SQLiteDatabase.OPEN_READWRITE);
			db.setLocale(Locale.getDefault());
			db.setLockingEnabled(true);
			db.setVersion(1);
			db.close();
		}catch(SQLiteException e){
			Log.e("SqlHelper", "Database Not Found");
		}
		
		return db != null ? true : false;
	}
	
	private void copyDBFromResources(String DatabasePath, String DatabaseName){
		
		InputStream inputStream = null;
		OutputStream outputStream = null;
		String DATABASE_PATH = DatabasePath;
		String DATABASE_NAME = DatabaseName;
		String dbfilePath = DATABASE_PATH + DATABASE_NAME;
		
		try{
			inputStream = myContext.getAssets().open(DATABASE_NAME);
			outputStream = new FileOutputStream(dbfilePath);
			byte[] buffer = new byte[1024];
			int length;
			while((length = inputStream.read(buffer)) > 0){
				outputStream.write(buffer, 0, length);
			}
			outputStream.flush();
			outputStream.close();
			inputStream.close();
		}catch(IOException e){
			throw new Error("Problem copying database from resource file");
		}
	}
	
	public void openDataBase(String DB_PATH, String DB_NAME) throws SQLException{
		 
        String myPath = DB_PATH + DB_NAME;
        dbSqlite = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);
 
    }
}
