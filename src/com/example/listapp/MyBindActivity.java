package com.example.listapp;

import com.example.listapp.MyBoundService.MyBinder;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MyBindActivity extends Activity{
	
	TextView tv;
	Button btn;
	MyBoundService mbs;
	boolean b;
	ServiceConnection sc = new ServiceConnection(){

		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			MyBinder binder = (MyBinder)service;
			mbs = binder.getService();
		}

		@Override
		public void onServiceDisconnected(ComponentName name) {
			b = false;
		}
	};
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.bind);
		tv = (TextView) findViewById(R.id.textView1);
		btn = (Button) findViewById(R.id.button1);
		btn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				tv.setText("System Time: "+mbs.getMySystemTime());
			}
		});
	}
	@Override
	protected void onStart() {
		super.onStart();
		Intent i = new Intent(this, MyBoundService.class);
		bindService(i, sc, BIND_AUTO_CREATE);
	}
	@Override
	protected void onStop() {
		super.onStop();
		if(b){
			mbs.unbindService(sc);
		}
		else{
			b=false;
		}
	}
}
