package com.example.listapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

public class Seekbar extends Activity implements OnSeekBarChangeListener{
	
	SeekBar sb;
	Button b;
	TextView tv;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.seek);
		tv = (TextView) findViewById(R.id.textView1);
		sb = (SeekBar) findViewById(R.id.seekBar1);
		b = (Button) findViewById(R.id.home);
		sb.setOnSeekBarChangeListener(this);
		b.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(Seekbar.this, listClass.class);
				startActivity(i);
			}
		});
	}

	@Override
	public void onProgressChanged(android.widget.SeekBar seekBar, int progress,
			boolean fromUser) {
		// TODO Auto-generated method stub
		
		Toast.makeText(this,"Progress is " + progress,Toast.LENGTH_SHORT).show();
		tv.setText("Progress is " + progress);
	}

	@Override
	public void onStartTrackingTouch(android.widget.SeekBar seekBar) {
		// TODO Auto-generated method stub
		Toast.makeText(this,"Progress is 1",Toast.LENGTH_SHORT).show();

	}

	@Override
	public void onStopTrackingTouch(android.widget.SeekBar seekBar) {
		// TODO Auto-generated method stub
		Toast.makeText(this,"Progress is 2",Toast.LENGTH_SHORT).show();

	}

}
