package com.example.listapp;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class DeleteData extends Activity{

	TextView tv;
	Button b;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.view);
		tv = (TextView) findViewById(R.id.textView1);
		b = (Button) findViewById(R.id.button1);
		b.setText("Delete Data");
		final SQLiteDatabase sq = openOrCreateDatabase("MyApp", MODE_WORLD_READABLE, null);
		Cursor c = sq.rawQuery("select * from emp", null);
		while(c.moveToNext()){
			tv.append("" + c.getInt(0) + " " + c.getString(1) + " " + c.getString(2) + "\n");
		}
		b.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				sq.execSQL("delete from emp");
				Intent i = new Intent(DeleteData.this, ViewData.class);
				startActivity(i);
			}
		});
	}
}
