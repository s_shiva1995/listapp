package com.example.listapp;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class ViewData extends Activity{

	TextView tv;
	Button b;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.view);
		tv = (TextView) findViewById(R.id.textView1);
//		SQLiteDatabase sq = openOrCreateDatabase("MyApp", MODE_WORLD_READABLE, null);
//		Cursor c = sq.rawQuery("select * from emp", null);
//		while(c.moveToNext()){
//			tv.append("" + c.getInt(0) + " " + c.getString(1) + " " + c.getString(2) + "\n");
//		}
		String str = "content://com.example.listapp.provider/";
		Uri uri = Uri.parse(str);
		ContentResolver r = getContentResolver();
		Cursor c = r.query(uri, null, null, null, null);
		while(c.moveToNext()){
			tv.append("" + c.getInt(0) + " " + c.getString(1) + " " + c.getString(2) + "\n");
		}
		b = (Button) findViewById(R.id.button1);
		b.setText("GOTO Delete Data Page");
		b.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent i = new Intent(ViewData.this, DeleteData.class);
				startActivity(i);
			}
		});
	}
	
}
