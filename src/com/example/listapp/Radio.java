package com.example.listapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.RadioButton;
import android.widget.Toast;

public class Radio extends Activity {
	
	RadioGroup rg;
	RadioButton rb1, rb2, rb3;
	Button home;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.radioexample);
		home = (Button) findViewById(R.id.home);
		rg = (RadioGroup) findViewById(R.id.radioGroup1);
		rg.clearCheck();
		OnClickListener l = new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				RadioButton rb = (RadioButton)v;
				Toast.makeText(Radio.this, "Value is " + Integer.toString(rb.getId()), Toast.LENGTH_SHORT).show();
			}
		};
		rb1 = (RadioButton) findViewById(R.id.radio0);
		rb2 = (RadioButton) findViewById(R.id.radio1);
		rb3 = (RadioButton) findViewById(R.id.radio2);
		rb1.setOnClickListener(l);
		rb2.setOnClickListener(l);
		rb3.setOnClickListener(l);
		home.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(Radio.this, listClass.class);
				startActivity(i);
			}
		});
	}

}
