package com.example.listapp;

import android.app.*;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.*;
import android.graphics.*;

//import java.util.Timer;
//import java.util.TimerTask;
//import java.util.concurrent.*;
import java.util.Random;

public class RandomBack extends Activity {

	Button b;
	LinearLayout ll;
	
//	public void pop(float i){
//		Toast.makeText(this, "done "+Float.toString(i), 2000).show();
//	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.random);
		b = (Button) findViewById(R.id.btnstart);
		ll = (LinearLayout) findViewById(R.id.ran);
		changeColor();
		b.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(RandomBack.this, listClass.class);
				startActivity(i);
			}
		});
//		Timer timer = new Timer();
//		timer.scheduleAtFixedRate(new TimerTask(){
//			@Override
//			public void run(){
//				Random rand = new Random();
//				float rand2 = rand.nextFloat();
//				System.out.println(rand2);
//				pop(rand2);
//				ll.setBackgroundColor(Color.parseColor("#333000"));
//			}
//		},4000,4000);
		
//		b.setOnClickListener(new View.OnClickListener() {
//			
//			@Override
//			public void onClick(View v) {
//				Random rand= new Random();
//				float color = rand.nextFloat();
//				System.out.println(color);
//				Timer timer = new Timer();
//				scheduleAtFixedRate
//				timer.schedule(new TimerTask(){
//					@Override
//					public void run(){
//						Random rand = new Random();
//						float rand2 = rand.nextFloat();
//						ll.setBackgroundColor(Color.argb(256, Math.round(color), Math.round(color), Math.round(color)));
//						try{
//						pop(rand2);
//						}
//						catch(Exception e)
//						{
//							e.printStackTrace();
//						}
//					}
//				},4000);
//				ll.setBackgroundColor(Color.parseColor("#333000"));
//			}
//		});
	}
	public void changeColor(){
		(new CountDownTimer(5000, 500) {
			
			@Override
			public void onTick(long millisUntilFinished) {
				// TODO Auto-generated method stub
				Random r = new Random();
				int  r1 = r.nextInt();
				int r2 = r.nextInt();
				int r3 = r.nextInt();
				ll.setBackgroundColor(Color.rgb(Math.round(r1), Math.round(r2), Math.round(r3)));
			}
			
			@Override
			public void onFinish() {
				// TODO Auto-generated method stub
				changeColor();
			}
		}).start();
	}
}
