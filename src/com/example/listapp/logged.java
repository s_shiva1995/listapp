package com.example.listapp;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.content.Intent;

public class logged extends Activity {

	TextView uname, upass;
	Button home;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.logged);
		uname = (TextView) findViewById(R.id.uname);
		upass = (TextView) findViewById(R.id.upass);
		home = (Button) findViewById(R.id.home);
		home.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(logged.this, listClass.class);
				startActivity(i);
			}
		});
		Intent id = getIntent();
		String u = id.getStringExtra("user");
		Intent pass = getIntent();
		String p = pass.getStringExtra("pass");
		uname.setText(u);
		upass.setText(p);
		
	}
	
}
