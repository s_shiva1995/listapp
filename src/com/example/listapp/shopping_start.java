package com.example.listapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class shopping_start extends Activity{

	Spinner sp;
	ImageView iv1, iv2;
	TextView tv1, tv2;
	Button b1, b2;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.shopping1);
		sp = (Spinner) findViewById(R.id.spinner1);
		iv1 = (ImageView) findViewById(R.id.imageView1);
		iv2 = (ImageView) findViewById(R.id.imageView2);
		tv1 = (TextView) findViewById(R.id.textView1);
		tv2 = (TextView) findViewById(R.id.textView2);
		b1 = (Button) findViewById(R.id.button1);
		b2 = (Button) findViewById(R.id.button2);
		sp.setOnItemSelectedListener(new OnItemSelectedListener(){

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				String item = sp.getSelectedItem().toString();
				Toast.makeText(shopping_start.this, item, Toast.LENGTH_SHORT).show();
				if(item.equals("Mobile")){
					iv1.setBackgroundResource(R.drawable.mob1);
					iv2.setBackgroundResource(R.drawable.mob2);
					tv1.setText("14000/-");
					tv2.setText("12000/-");
				}
				else if(item.equals("Laptop")){
					iv1.setBackgroundResource(R.drawable.lap1);
					iv2.setBackgroundResource(R.drawable.lap2);
					tv1.setText("40000/-");
					tv2.setText("38000/-");
				}
				else if(item.equals("Earphones")){
					iv1.setBackgroundResource(R.drawable.ear1);
					iv2.setBackgroundResource(R.drawable.ear2);
					tv1.setText("2000/-");
					tv2.setText("1500/-");
				}
				else if(item.equals("Tablet")){
					iv1.setBackgroundResource(R.drawable.tab1);
					iv2.setBackgroundResource(R.drawable.tab2);
					tv1.setText("14000/-");
					tv2.setText("12000/-");
				}
				else if(item.equals("Television")){
					iv1.setBackgroundResource(R.drawable.tv1);
					iv2.setBackgroundResource(R.drawable.tv2);
					tv1.setText("44000/-");
					tv2.setText("40000/-");
				}
				else if(item.equals("Air Conditioner")){
					iv1.setBackgroundResource(R.drawable.air1);
					iv2.setBackgroundResource(R.drawable.air2);
					tv1.setText("30000/-");
					tv2.setText("24000/-");
				}
				else if(item.equals("Clock")){
					iv1.setBackgroundResource(R.drawable.clock1);
					iv2.setBackgroundResource(R.drawable.clock2);
					tv1.setText("1000/-");
					tv2.setText("2000/-");
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}});
		b1.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String item = sp.getSelectedItem().toString();
				Intent i = new Intent(shopping_start.this, shopping_confirm.class);
				if(item.equals("Mobile")){
					i.putExtra("product", "Mobile");
					//i.putExtra("pro_num", 0);
				}
				else if(item.equals("Laptop")){
					i.putExtra("product", "Laptop");
					//i.putExtra("pro_num", 0);
				}
				else if(item.equals("Earphones")){
					i.putExtra("product", "Earphones");
					//i.putExtra("pro_num", 0);
				}
				else if(item.equals("Tablet")){
					i.putExtra("product", "Tablet");
					//i.putExtra("pro_num", 0);
				}
				else if(item.equals("Television")){
					i.putExtra("product", "Television");
					//i.putExtra("pro_num", 0);
				}
				else if(item.equals("Air Conditioner")){
					i.putExtra("product", "Air Conditioner");
					//i.putExtra("pro_num", 0);
				}
				else if(item.equals("Clock")){
					i.putExtra("product", "Clock");
					//i.putExtra("pro_num", 0);
				}
				i.putExtra("pro_num", 0);
				startActivity(i);
				
			}
		});
		b2.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String item = sp.getSelectedItem().toString();
				Intent i = new Intent(shopping_start.this, shopping_confirm.class);
				if(item.equals("Mobile")){
					i.putExtra("product", "Mobile");
					//i.putExtra("pro_num", 1);
				}
				else if(item.equals("Laptop")){
					i.putExtra("product", "Laptop");
					//i.putExtra("pro_num", 1);
				}
				else if(item.equals("Earphones")){
					i.putExtra("product", "Earphones");
					//i.putExtra("pro_num", 1);
				}
				else if(item.equals("Tablet")){
					i.putExtra("product", "Tablet");
					//i.putExtra("pro_num", 1);
				}
				else if(item.equals("Television")){
					i.putExtra("product", "Television");
					//i.putExtra("pro_num", 1);
				}
				else if(item.equals("Air Conditioner")){
					i.putExtra("product", "Air Conditioner");
					//i.putExtra("pro_num", 1);
				}
				else if(item.equals("Clock")){
					i.putExtra("product", "Clock");
					//i.putExtra("pro_num", 1);
				}
				i.putExtra("pro_num", 1);
				startActivity(i);
			}
		});
	}
	
}
