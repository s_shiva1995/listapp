package com.example.listapp;

import android.app.*;
import android.os.Bundle;
import android.graphics.*;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;
import android.content.*;

public class MyWallpaper extends Activity implements OnClickListener {
	
	Button b;
	ImageView iv;
	int pic;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.wall);
		b = (Button) findViewById(R.id.button1);
		iv = (ImageView) findViewById(R.id.tv);
		pic = R.drawable.ic_launcher;
		ImageView v1 = (ImageView) findViewById(R.id.imageView1);
		ImageView v2 = (ImageView) findViewById(R.id.imageView2);
		ImageView v3 = (ImageView) findViewById(R.id.imageView3);
		ImageView v4 = (ImageView) findViewById(R.id.imageView4);
		ImageView v5 = (ImageView) findViewById(R.id.imageView5);
		b.setOnClickListener(this);
		v1.setOnClickListener(this);
		v2.setOnClickListener(this);
		v3.setOnClickListener(this);
		v4.setOnClickListener(this);
		v5.setOnClickListener(this);
		
	}
	public void home(View v){
		Intent i = new Intent(MyWallpaper.this, listClass.class);
		startActivity(i);
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		case R.id.imageView1:
			iv.setBackgroundResource(R.drawable.pic1);
			pic = R.drawable.pic1;
			break;
		case R.id.imageView2:
			iv.setBackgroundResource(R.drawable.pic2);
			pic = R.drawable.pic2;
			break;
		case R.id.imageView3:
			iv.setBackgroundResource(R.drawable.pic3);
			pic = R.drawable.pic3;
			break;
		case R.id.imageView4:
			iv.setBackgroundResource(R.drawable.pic4);
			pic = R.drawable.pic4;
			break;
		case R.id.imageView5:
			iv.setBackgroundResource(R.drawable.pic5);
			pic = R.drawable.pic5;
			break;
		default:
			Bitmap bitmap = BitmapFactory.decodeStream(getResources().openRawResource(pic));
			try{
				getApplicationContext().setWallpaper(bitmap);
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
	}
}
