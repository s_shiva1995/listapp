package com.example.listapp;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.View;

public class MyView extends View{
	
	Bitmap b;
	Paint p = new Paint();
	int x = 0, y = 0;
	Context c;
	boolean flag = true;
	Rect r = new Rect();

	public MyView(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		b = BitmapFactory.decodeResource(getResources(), R.drawable.ic_launcher);
	}
	
	public void drawPic(int x2, int y2, Canvas c){
		c.drawBitmap(b, x2, y2, p);
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
		// TODO Auto-generated method stub
		super.onDraw(canvas);
		r.set(0, 0, canvas.getHeight(), canvas.getWidth());
		p.setColor(Color.WHITE);
		p.setStyle(Paint.Style.FILL);
		canvas.drawRect(r, p);
		x = (canvas.getWidth() / 2) - b.getHeight();
		drawPic(x, y, canvas);
		//drawPic(10, 100, canvas);
		if(flag || !flag){
			if(flag){
				if(y > canvas.getHeight() - 100)
					flag = false;
				else{
					y += 2;
					invalidate();
				}
			}
			if(!flag){
				if(y < 0)
					flag = true;
				else{
					y -= 2;
					invalidate();
				}
			}
			invalidate();
		}
	}

}
