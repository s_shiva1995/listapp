package com.example.listapp;

import android.app.*;
import android.content.*;
import android.os.Bundle;
import android.view.*;
import android.widget.*;

public class MyMusicActivity extends Activity {
	
	Button start, stop, pause ,home;
	Intent intent;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.mymusiclayout);
		start = (Button) findViewById(R.id.start);
		stop = (Button) findViewById(R.id.stop);
		pause = (Button) findViewById(R.id.pause);
		intent = new Intent(MyMusicActivity.this, MyMusicService.class);
		home = (Button) findViewById(R.id.home);
		home.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(MyMusicActivity.this, listClass.class);
				startActivity(i);
			}
		});
	}
	public void start(View v){
		startService(intent);
	}
	public void stop(View v){
		stopService(intent);
	}
	public void pause(View v){
		
		
	}

}
