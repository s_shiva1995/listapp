package com.example.listapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;

public class AutoCompleteEx extends Activity{
	
	AutoCompleteTextView actv;
	Button b;
	String[] names = {"India","Pakistan","Afghanistan","China","Nepal","Australia","United States"};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.autocomplete);
		b = (Button) findViewById(R.id.home);
		b.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				Intent i = new Intent(AutoCompleteEx.this, listClass.class);
				startActivity(i);
				
			}
		});
		actv = (AutoCompleteTextView) findViewById(R.id.autoCompleteTextView1);
		ArrayAdapter<String> aa = new ArrayAdapter<String>(this, android.R.layout.simple_expandable_list_item_1, names);
		actv.setAdapter(aa);
		actv.setThreshold(0);
		
	}

}
