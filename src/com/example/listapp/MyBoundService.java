package com.example.listapp;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import java.util.Date;

public class MyBoundService extends Service{

	private Binder binder = new MyBinder();
	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return binder;
	}
	public class MyBinder extends Binder {
		
		MyBoundService getService(){
			return MyBoundService.this;
		}
	}
	public String getMySystemTime(){
		
		return new Date(System.currentTimeMillis()).toString();
	}

}
