package com.example.listapp;

import android.app.Activity;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class InsertData extends Activity {

	EditText tv1, tv2, tv3;
	Button b;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.insert);
		tv1 = (EditText) findViewById(R.id.editText1);
		tv2 = (EditText) findViewById(R.id.editText2);
		tv3 = (EditText) findViewById(R.id.editText3);
		b = (Button) findViewById(R.id.button1);
		final SQLiteDatabase sq = openOrCreateDatabase("MyApp", MODE_WORLD_WRITEABLE, null);
		sq.execSQL("create table if not exists emp(eid int(5), ename varchar(20), email varchar(20))");
		b.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				String eid = tv1.getText().toString();
				int eid2 = Integer.parseInt(eid);
				String ename = tv2.getText().toString();
				String email = tv3.getText().toString();
				sq.execSQL("insert into emp values("+eid2+", '"+ename+"', '"+email+"')");
				Intent i = new Intent(InsertData.this, ViewData.class);
				startActivity(i);
			}
		});
		
	}
}
