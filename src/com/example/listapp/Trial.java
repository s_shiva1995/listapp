package com.example.listapp;

import android.app.Activity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

public class Trial extends Activity{
	
	SQLiteDatabase db;
	TextView tv;
	Button b;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.trial);
		tv = (TextView) findViewById(R.id.textView1);
		b = (Button) findViewById(R.id.button1);
		tv.setScrollbarFadingEnabled(true);
		//db = openOrCreateDatabase("9000series.sqlite" , MODE_WORLD_READABLE, null);
    	db = SQLiteDatabase.openDatabase("/data/data/com.example.listapp/databases/9000series.sqlite", null, SQLiteDatabase.OPEN_READONLY);
		Cursor c = db.rawQuery("select * from series9000", null);
		while(c.moveToNext()){
			tv.append("" + c.getInt(0) + " " + c.getString(1) + " " + c.getString(2) + "\n");
		}
	}
	
}
