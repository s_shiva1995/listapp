package com.example.listapp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.widget.Toast;

public class MyPhoneReceive extends BroadcastReceiver {

	static String in;
	static String _state, courier;
	SQLiteDatabase db;
	
	@Override
	//public void onReceive(final Context context, final Intent intent) {
	public void onReceive(final Context context, Intent intent) {
		
		TelephonyManager telephony = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
		
        telephony.listen(new PhoneStateListener(){
        	
            @Override
            public void onCallStateChanged(int state, String incomingNumber) {
                super.onCallStateChanged(state, incomingNumber);
                
                in = incomingNumber;
                String s = null, c = null;
                String[] num = incomingNumber.split("");
                int numCheck = Integer.parseInt(num[4] + num[5] + num[6] + num[7]);
                
                switch(Integer.parseInt(num[4])){
                	case 9:
	                	db = SQLiteDatabase.openDatabase("/data/data/com.example.listapp/databases/9000series.sqlite", null, SQLiteDatabase.OPEN_READONLY);
	                	Cursor c0 = db.rawQuery("select * from series9000 where DIGIT = "+numCheck+"", null);
	                	while(c0.moveToNext()){
	                		//in = in + " " + c0.getString(1) + " " + c0.getString(2);
	                		c = c0.getString(1);
	                		s = c0.getString(2);
	                	}
	                	db.close();
	                	break;
                	case 8: 
	                	db = SQLiteDatabase.openDatabase("/data/data/com.example.listapp/databases/8000series.sqlite", null, SQLiteDatabase.OPEN_READONLY);
	                	Cursor c1 = db.rawQuery("select * from series8000 where DIGIT = "+numCheck+"", null);
	                	while(c1.moveToNext()){
	                		//in = in + " " + c1.getString(1) + " " + c1.getString(2);
	                		c = c1.getString(1);
	                		s = c1.getString(2);
	                	}
	                	db.close();
	                	break;
                	case 7: 
	                	db = SQLiteDatabase.openDatabase("/data/data/com.example.listapp/databases/7000series.sqlite", null, SQLiteDatabase.OPEN_READONLY);
	                	Cursor c2 = db.rawQuery("select * from series7000 where DIGIT = "+numCheck+"", null);
	                	while(c2.moveToNext()){
	                		//in = in + " " + c2.getString(1) + " " + c2.getString(2);
	                		c = c2.getString(1);
	                		s = c2.getString(2);
	                	}
	                	db.close();
	                	break;
                }
                
                if(s.equals("AP"))
        			_state = "Andhra Pradesh Telecom Circles";
        		else if(s.equals("AS"))
        			_state = "Assam Telecom Circles";
        		else if(s.equals("BR"))
        			_state = "Bihar Telecom Circles";
        		else if(s.equals("CH"))
        			_state = "Chennai Telecom Circles";
        		else if(s.equals("DL"))
        			_state = "Delhi Telecom Circles";
        		else if(s.equals("GJ"))
        			_state = "Gujarat Telecom Circles";
        		else if(s.equals("HP"))
        			_state = "Himanchal Pradesh Telecom Circles";
        		else if(s.equals("HR"))
        			_state = "Haryana Telecom Circles";
        		else if(s.equals("JK"))
        			_state = "Jammu and Kashmir Telecom Circles";
        		else if(s.equals("KL"))
        			_state = "Kerala Telecom Circles";
        		else if(s.equals("KA"))
        			_state = "Karnataka Telecom Circles";
        		else if(s.equals("KO"))
        			_state = "Kolkata Telecom Circles";
        		else if(s.equals("MH"))
        			_state = "Maharashtra Telecom Circles";
        		else if(s.equals("MP"))
        			_state = "Madhya Pradesh and Chattisgarh Telecom Circles";
        		else if(s.equals("MU"))
        			_state = "Mumbai Telecom Circles";
        		else if(s.equals("NE"))
        			_state = "North East India Telecom Circles";
        		else if(s.equals("OR"))
        			_state = "Odisha Telecom Circles";
        		else if(s.equals("PB"))
        			_state = "Punjab Telecom Circles";
        		else if(s.equals("RJ"))
        			_state = "Rajasthan Telecom Circles";
        		else if(s.equals("TN"))
        			_state = "Tamil Nadu Telecom Circles";
        		else if(s.equals("UE"))
        			_state = "Uttar Pradesh(East) Telecom Circles";
        		else if(s.equals("UW"))
        			_state = "Uttar Pradesh(West) Telecom Circles";
        		else if(s.equals("WB"))
        			_state = "West Bengal Telecom Circles";
        		else if(s.equals("ZZ"))
        			_state = "Customer Care";
        		else
        			_state = "No Entry";
                
                if(c.equals("AC"))
        			courier = "Aircel";
        		else if(c.equals("AT"))
        			courier = "Bharti Airtel";
        		else if(c.equals("CC"))
        			courier = "BSNL CDMA";
        		else if(c.equals("CG"))
        			courier = "BSNL GSM";
        		else if(c.equals("DC"))
        			courier = "Videocon";
        		else if(c.equals("DP"))
        			courier = "MTNL";
        		else if(c.equals("ET"))
        			courier = "Etisalat India";
        		else if(c.equals("ID"))
        			courier = "Idea";
        		else if(c.equals("LM"))
        			courier = "Loop Mobile";
        		else if(c.equals("MT"))
        			courier = "MTS";
        		else if(c.equals("PG"))
        			courier = "PING CDMA";
        		else if(c.equals("RC"))
        			courier = "Reliance CDMA";
        		else if(c.equals("RG"))
        			courier = "Reliance GSM";
        		else if(c.equals("RJ"))
        			courier = "Reliance Jio Infocomm";
        		else if(c.equals("SP"))
        			courier = "Spice Digital";
        		else if(c.equals("ST"))
        			courier = "S Tel";
        		else if(c.equals("T24"))
        			courier = "T24(BIG BAZAR)";
        		else if(c.equals("TD"))
        			courier = "Tata Docomo";
        		else if(c.equals("TI"))
        			courier = "Tata Indicom";
        		else if(c.equals("UN"))
        			courier = "Uninor";
        		else if(c.equals("VC"))
        			courier = "Virgin Mobile CDMA";
        		else if(c.equals("VG"))
        			courier = "Virgin Mobile GSM";
        		else if(c.equals("VF"))
        			courier = "Vodafone";
        		else if(c.equals("VD"))
        			courier = "Videocon";
        		else
        			courier = "No Entry";
                
//     	        if(state == 1){  
//     	              Toast.makeText(context, "Ringing : " + in + " : " + _state + " " + courier, Toast.LENGTH_LONG).show();  
//     	        }  
//     	        else if(state == 0){
//    	              Toast.makeText(context, "Idle : " + in + " : " + _state + " " + courier, Toast.LENGTH_LONG).show();
//     	        }
//     	        else
//     	              Toast.makeText(context, "No operation" + state + incomingNumber, Toast.LENGTH_LONG).show();
                
            }
        },PhoneStateListener.LISTEN_CALL_STATE);
        
		try{  
	           String state = intent.getStringExtra(TelephonyManager.EXTRA_STATE);  
	  
	              if(state.equals(TelephonyManager.EXTRA_STATE_RINGING)){  
	                   Toast.makeText(context, "Ringing : " + in + " : " + _state + " " + courier, Toast.LENGTH_LONG).show();  
	              }  
	                
	              if(state.equals(TelephonyManager.EXTRA_STATE_OFFHOOK)){  
	                   Toast.makeText(context, "Recieved :" + in + " : " + _state + " " + courier, Toast.LENGTH_LONG).show();  
	              }  
	                
	              if (state.equals(TelephonyManager.EXTRA_STATE_IDLE)){  
	                   Toast.makeText(context, "Idle :" + in + " : " + _state + " " + courier, Toast.LENGTH_LONG).show();  
	              }  
	          }  
	          catch(Exception e){
	        	  e.printStackTrace();
	        	  } 
	    }
	
}
