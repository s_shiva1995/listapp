package com.example.listapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ProgressBar;

public class shopping_splash extends Activity{
	
	ProgressBar pb;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.shopping_splash);
		pb = (ProgressBar) findViewById(R.id.progressBar1);
		pb.postDelayed(new Runnable(){

			@Override
			public void run() {
				// TODO Auto-generated method stub
				Intent i = new Intent(shopping_splash.this, shopping_start.class);
				startActivity(i);
				finish();
			}
			
			
		}, 3000);
		
	}

}
