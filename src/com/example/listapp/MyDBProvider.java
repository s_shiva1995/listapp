package com.example.listapp;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

public class MyDBProvider extends ContentProvider {

	@Override
	public int delete(Uri arg0, String arg1, String[] arg2) {
		// TODO Auto-generated method stub
		Context c = getContext();
		SQLiteDatabase sq = c.openOrCreateDatabase("MyApp", Context.MODE_WORLD_WRITEABLE, null);
		int m = sq.delete("emp", arg1, arg2);
		return m;
	}

	@Override
	public String getType(Uri uri) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		// TODO Auto-generated method stub
		Context c = getContext();
		SQLiteDatabase sq = c.openOrCreateDatabase("MyApp", Context.MODE_WORLD_WRITEABLE, null);
		sq.insert("emp", null, values);
		return uri;
	}

	@Override
	public boolean onCreate() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {
		// TODO Auto-generated method stub
		Context c = getContext();
		SQLiteDatabase sq = c.openOrCreateDatabase("MyApp", Context.MODE_WORLD_WRITEABLE, null);
		Cursor cursor = sq.rawQuery("select * from emp", selectionArgs);
		return cursor;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {
		// TODO Auto-generated method stub
		return 0;
	}

}
