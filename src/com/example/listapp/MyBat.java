package com.example.listapp;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.widget.ProgressBar;
import android.widget.TextView;

public class MyBat extends Activity {
	
	private BroadcastReceiver r = new BroadcastReceiver(){

		@Override
		public void onReceive(Context arg0, Intent intent) {
			// TODO Auto-generated method stub
			
			int level = intent.getIntExtra("level", 10);
			ProgressBar pb = (ProgressBar)findViewById(R.id.progressBar1);
			pb.setProgress(level);
			TextView tv = (TextView)findViewById(R.id.textView1);
			tv.setText("Battery level: " + Integer.toString(level));
			
		}};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.mybat);
		registerReceiver(r, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
	}}
