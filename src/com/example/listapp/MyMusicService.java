package com.example.listapp;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.IBinder;

public class MyMusicService extends Service implements OnCompletionListener{

	MediaPlayer mp;
	
	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}

	@Override
	public void onCompletion(MediaPlayer arg0) {
		mp.release();
	}
	
	@Override
	public void onCreate() {
		super.onCreate();
		mp = MediaPlayer.create(this, R.raw.music);
	}
	
//	private void onPause() {
//		if(mp.isPlaying()){
//			mp.pause();
//		}
//	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		if(!mp.isLooping()){
			mp.start();
		}
		return START_STICKY;
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		if(mp.isPlaying()){
			mp.stop();
		}
	}
}
