package com.example.listapp;

import android.app.*;
import android.os.Bundle;
import android.view.*;
import android.content.*;
import android.graphics.Color;
import android.widget.*;

public class login extends Activity{
	
	EditText email, pass;
	Button submit;
	RelativeLayout rl;
	//SharedPreferences sp;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.loginview);
	    rl = (RelativeLayout) findViewById(R.id.lay);
		email = (EditText) findViewById(R.id.userid);
		pass = (EditText) findViewById(R.id.password);
		submit = (Button) findViewById(R.id.submit);
		//sp = getSharedPreferences("MM", MODE_PRIVATE);
		
		submit.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String uname = email.getText().toString();
				String upass = pass.getText().toString();
				boolean passw = false;
				if(upass.length() >= 8){
					for(int o = 0; o <= 9; o++){
						if(upass.contains(Integer.toString(o))){
							for(int p = 97; p <= 122; p++){
								char i = (char)p;
								if(upass.contains(Character.toString(i))){
									for(int k = 65; k <= 90; k++){
										char l = (char)k;
										if(upass.contains(Character.toString(l))){
											passw = true;
										break;
											}
										}
									break;
								}
							}
							break;
						}
					}
				}
				if(uname.endsWith(".com")&&uname.contains("@")&&passw){
					rl.setBackgroundColor(Color.parseColor("#33CC33"));
					Intent i = new Intent(login.this, logged.class);
					String user = email.getText().toString();
					String pass2 = pass.getText().toString();
					i.putExtra("user", user);
					i.putExtra("pass", pass2);
					toast(true);
					startActivity(i);
					}
				else {
					rl.setBackgroundColor(Color.parseColor("#FF3300"));
					toast(false);
					}
				}
		});
	}
	public void toast(boolean i){
		if(i){
			Toast.makeText(this, "Valid", 3000).show();
//			char frmascii = (char)97-122;
//			char p = 'Z';
//			int ascii = (int)p;
			//Toast.makeText(this, Integer.toString(ascii), 3000).show();
			}
		else{
			Toast.makeText(this, "Invalid", 3000).show();
		}
		
	}
	public void home(View v){
		Intent i = new Intent(login.this, listClass.class);
		startActivity(i);
	}
}
