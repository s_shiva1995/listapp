package com.example.listapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;
import android.widget.ToggleButton;
import android.widget.Button;

public class Toggle extends Activity {

	Button home;
	ToggleButton tb;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.toggle);
		home = (Button) findViewById(R.id.home);
		tb = (ToggleButton) findViewById(R.id.toggle);
		tb.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Toast.makeText(Toggle.this, "Button is " + tb.getText(), Toast.LENGTH_LONG).show();
			}
		});
		home.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(Toggle.this, listClass.class);
				startActivity(i);
			}
		});
	}
	
}
