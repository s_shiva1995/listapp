package com.example.listapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class shopping_confirm extends Activity{
	
	ImageView iv;
	Button b1, b2;
	TextView tv;
	int bill;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.shopping2);
		iv = (ImageView) findViewById(R.id.imageView1);
		tv = (TextView) findViewById(R.id.textView1);
		b1 = (Button) findViewById(R.id.home);
		b1.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(shopping_confirm.this, shopping_start.class);
				startActivity(i);
			}
		});
		b2 = (Button) findViewById(R.id.btncnfrm);
		Intent image = getIntent();
		String item = image.getStringExtra("product");
		Intent num = getIntent();
		int j = num.getIntExtra("pro_num", 1);
		Toast.makeText(this, item, Toast.LENGTH_SHORT).show();
		Toast.makeText(this, Integer.toString(j), Toast.LENGTH_SHORT).show();
		if(item.equals("Mobile")){
			if(j == 0){
				iv.setBackgroundResource(R.drawable.mob1);
				tv.setText("14000/-");
				bill = 14000;
			}
			else{
				iv.setBackgroundResource(R.drawable.mob2);
				tv.setText("12000/-");
				bill = 12000;
			}
		}
		else if(item.equals("Laptop")){
			if(j == 0){
				iv.setBackgroundResource(R.drawable.lap1);
				tv.setText("40000/-");
				bill = 40000;
				}
			else{
				iv.setBackgroundResource(R.drawable.lap2);
				tv.setText("38000/-");
				bill = 38000;
				}
		}
		else if(item.equals("Earphones")){
			if(j == 0){
				iv.setBackgroundResource(R.drawable.ear1);
				tv.setText("2000/-");
				bill = 2000;
			}
			else{
				iv.setBackgroundResource(R.drawable.ear2);
				tv.setText("1500/-");
				bill = 1500;
			}
		}
		else if(item.equals("Tablet")){
			if(j == 0){
				iv.setBackgroundResource(R.drawable.tab1);
				tv.setText("14000/-");
				bill = 14000;
			}
			else{
				iv.setBackgroundResource(R.drawable.tab2);
				tv.setText("12000/-");
				bill = 12000;
			}
		}
		else if(item.equals("Television")){
			if(j == 0){
				iv.setBackgroundResource(R.drawable.tv1);
				tv.setText("44000/-");
				bill = 44000;
			}
			else {
				iv.setBackgroundResource(R.drawable.tv2);
				tv.setText("40000/-");
				bill = 40000;
			}
		}
		else if(item.equals("Air Conditioner")){
			if(j == 0){
			iv.setBackgroundResource(R.drawable.air1);
			tv.setText("30000/-");
			bill = 30000;
			}
			else{
				iv.setBackgroundResource(R.drawable.air2);
				tv.setText("24000/-");
				bill = 24000;
			}
		}
		else if(item.equals("Clock")){
			if(j == 0){
				iv.setBackgroundResource(R.drawable.clock1);
				tv.setText("1000/-");
				bill = 1000;
			}
			else {
				iv.setBackgroundResource(R.drawable.clock2);
				tv.setText("2000/-");
				bill = 2000;
			}
		}
		b2.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(shopping_confirm.this, shopping_end.class);
				i.putExtra("bill", bill);
				startActivity(i);
			}
		});
	}
	
}
