package com.example.listapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;

public class Progress extends Activity {
	
	Button b;
	ProgressBar pb;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.progress);
		b = (Button) findViewById(R.id.home);
		b.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(Progress.this, listClass.class);
				startActivity(i);
			}
		});
		pb = (ProgressBar) findViewById(R.id.progressBar1);
		Thread t = new Thread(){
			public void run(){
				for(int i = 0; i <= 100; i += 5)
				{
					pb.setProgress(i);
					try{
						sleep(500);
					}catch(Exception e){
						e.printStackTrace();
					}
				}
			}
		};
		t.start();
	}

}
